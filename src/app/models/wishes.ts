export interface Wishes {
  status:             string;
  createdDate:        string;
  volunteerProfile:   VolunteerProfile;
  coordinatorProfile: CoordinatorProfile;
  initiative:         Initiative;
  entries?:           Entries[];
  id:                 string;
}

export interface VolunteerProfile {
  firstname: string;
  lastname:  string;
  company:   Company;
}

export interface Company {
  name: string;
}

export interface CoordinatorProfile {
  firstname: string;
  lastname:  string;
}

export interface Initiative {
  title:      string;
  streetName: string;
  city:       string;
  postalCode: string;
  country:    string;
}

export interface Entries {
  dateBegin: string;
  dateEnd:  string;
}
