export interface Stats {
  status: string;
  count:  number;
  label:  string;
  color:  string;
}
