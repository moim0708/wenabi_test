import { Component, OnDestroy, OnInit } from '@angular/core';
import { StatsService, StatsState } from 'src/app/services/stats.service';

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.scss']
})
export class StatsComponent implements OnInit, OnDestroy {
  public localStatsState: StatsState | null = null ;

  constructor(private statsService: StatsService) {
    this.statsService.state$.subscribe((state) => {
      this.localStatsState = state;
      // console.table(this.localStatsState);
    });
  }

  ngOnInit() {
    this.statsService.getStats();
  }

  ngOnDestroy(): void {
    this.statsService.state$.unsubscribe();
  }
}
