import { Component, Input, OnInit } from "@angular/core";
import { Stats } from "src/app/models/stats";
import { StatsService } from "src/app/services/stats.service";
import { WishesService } from "src/app/services/wishes.service";

@Component({
  selector: "app-status",
  templateUrl: "./status.component.html",
  styleUrls: ["./status.component.scss"],
})
export class StatusComponent implements OnInit {
  @Input() stats: Stats | null = null;
  @Input() currentStatus: string = null;

  constructor(
    private wishesService: WishesService,
    private statsService: StatsService
  ) {}

  ngOnInit() {}

  public changeStatus() {
    console.log('changeStatus', this.stats.status);
    if(this.stats.status == this.currentStatus){
      this.wishesService.getWishes();
      this.statsService.changeStatus(null);
    }else{
      this.wishesService.getWishesFromStatus(this.stats.status);
      this.statsService.changeStatus(this.stats.status);
    }
  }
}
