import { Component, Input, OnInit } from '@angular/core';
import { Stats } from 'src/app/models/stats';

@Component({
  selector: 'app-progress-bar',
  templateUrl: './progress-bar.component.html',
  styleUrls: ['./progress-bar.component.scss']
})
export class ProgressBarComponent implements OnInit {
  @Input() stats: Stats | null = null;
  @Input() totalCount: number | null = null;

  constructor() { }

  ngOnInit() {
  }

}
