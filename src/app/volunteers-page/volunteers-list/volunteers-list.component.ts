import { Component, OnDestroy, OnInit } from "@angular/core";
import { WishesService, WishesState } from "src/app/services/wishes.service";

@Component({
  selector: "app-volunteers-list",
  templateUrl: "./volunteers-list.component.html",
  styleUrls: ["./volunteers-list.component.scss"],
})
export class VolunteersListComponent implements OnInit, OnDestroy {
  public localWishesState: WishesState | null = null ;

  constructor(private wishesService: WishesService) {
    this.wishesService.state$.subscribe((state) => {
      this.localWishesState = state;
      //console.table(this.localWishes);
    });
  }

  ngOnInit() {
    this.wishesService.getWishes();
  }

  ngOnDestroy(): void {
    this.wishesService.state$.unsubscribe();
  }
}
