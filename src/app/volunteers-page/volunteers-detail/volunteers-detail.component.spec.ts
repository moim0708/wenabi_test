import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VolunteersDetailComponent } from './volunteers-detail.component';

describe('VolunteersDetailComponent', () => {
  let component: VolunteersDetailComponent;
  let fixture: ComponentFixture<VolunteersDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VolunteersDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VolunteersDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
