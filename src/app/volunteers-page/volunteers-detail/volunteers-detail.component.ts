import { Component, Input, OnInit } from "@angular/core";
import { Wishes } from "src/app/models/wishes";
import { StatsService } from "src/app/services/stats.service";

@Component({
  selector: "app-volunteers-detail",
  templateUrl: "./volunteers-detail.component.html",
  styleUrls: ["./volunteers-detail.component.scss"],
})
export class VolunteersDetailComponent implements OnInit {
  @Input() wishes: Wishes | null = null;
  public randomPictAvatar:number = 1 //Math.ceil(Math.random() * 7);

  constructor(public statsService: StatsService) {}

  ngOnInit() {}

  getCalcTime(start: string, end: string): string {
    let dateStart: number = new Date(start).getTime();
    let dateEnd: number = new Date(end).getTime();
    let secPass: number = (dateEnd - dateStart) / 1000;
    return this.convertHMS(secPass);
  }

  convertHMS(value):string {
    const sec = parseInt(value, 10);
    let hours: any = Math.floor(sec / 3600);
    let minutes: any = Math.floor((sec - hours * 3600) / 60);
    if (minutes < 10) {
      minutes = "0" + minutes;
    }
    return hours + "h" + minutes;
  }
}
