import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VolunteersPageComponent } from './volunteers-page.component';
import { StatsComponent } from '../volunteers-page/stats/stats.component';
import { StatusComponent } from './stats/status/status.component';
import { VolunteersListComponent } from './volunteers-list/volunteers-list.component';
import { VolunteersDetailComponent } from './volunteers-detail/volunteers-detail.component';
import { ProgressBarComponent } from './stats/progress-bar/progress-bar.component';



@NgModule({
  declarations: [
    VolunteersPageComponent,
    StatsComponent,
    StatusComponent,
    VolunteersListComponent,
    VolunteersDetailComponent,
    ProgressBarComponent
  ],
  imports: [
    CommonModule
  ]
})
export class VolunteersPageModule { }
