import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VolunteersPageComponent } from './volunteers-page/volunteers-page.component';


const routes: Routes = [
  {
    path: '',
    component:VolunteersPageComponent
  },
  {
    path: '**',
    redirectTo: '/',
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
