import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject } from "rxjs";
import { Wishes } from "../models/wishes";

export interface WishesState {
  wishes: Wishes[];
  loading: boolean;
}

@Injectable({
  providedIn: "root",
})
export class WishesService {
  /* API URLs */
  private API_WISHES = "https://6143a763c5b553001717d062.mockapi.io/api/wishes";

  /* Internal Data State */
  private wishes: Wishes[] = [];
  private loading = false;

  /* Public Presentational State */
  public state$ = new BehaviorSubject<WishesState>({
    wishes: this.wishes,
    loading: this.loading,
  });

  constructor(private http: HttpClient) {}

  /* Service API*/
  public getWishes(): void {
    this.loading = true;
    this.resfreshState();
    //
    this.http
      .get<any>(this.API_WISHES)
      .subscribe((data) => {
        this.wishes = data;
        this.loading = false;
        this.resfreshState();
      });
  }

  public getWishesFromStatus(status:string): void {
    this.loading = true;
    this.resfreshState();
    //
    this.http
      .get<any>(this.API_WISHES + '?status=' + status)
      .subscribe((data) => {
        this.wishes = data;
        this.loading = false;
        this.resfreshState();
      });
  }

  //

  private resfreshState(): void {
    this.state$.next({
      wishes: this.wishes,
      loading: this.loading,
    });
  }
}
