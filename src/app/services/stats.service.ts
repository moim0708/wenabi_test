import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { Stats } from "../models/stats";

export interface StatsState {
  stats: Stats[];
  currentStatus:string | null,
  totalCount:number,
  loading: boolean;
}

@Injectable({
  providedIn: "root",
})
export class StatsService {
  /* API URLs */
  private API_STATS = "https://6143a763c5b553001717d062.mockapi.io/api/stats";

  /* Internal Data State */
  private stats: Stats[] = [];
  private currentStatus:string | null = null;
  private totalCount:number = 0;
  private loading = false;

  /* Public Presentational State */
  public state$ = new BehaviorSubject<StatsState>({
    stats: this.stats,
    currentStatus:this.currentStatus,
    totalCount:this.totalCount,
    loading: this.loading,
  });

  constructor(private http: HttpClient) {}

  /* Service API*/
  public getStats(): void {
    this.loading = true;
    this.resfreshState();
    //
    this.http.get<any>(this.API_STATS).subscribe((data) => {
      this.stats = data;
      this.stats.map((stat) => {
        stat.label = this.getLabel(stat.status);
        stat.color = this.getColor(stat.status);
        this.totalCount += stat.count;
      });
      this.loading = false;
      this.resfreshState();
    });
  }

  //

  public changeStatus(status:string):void{
    this.currentStatus = status;
    this.resfreshState();
  }

  public getLabel(status:string): string{
    let label:string;
    switch (status) {
      case 'APPLICATION':                     label = "Candidature";                      break;
      case 'DISCUSSION':                      label = "Échange";                          break;
      case 'WAITING_ASSOCIATION_VALIDATION':  label = "Date à valider";                   break;
      case 'WAITING_MANAGER_VALIDATION':      label = "En attente du manager";            break;
      case 'IN_PROGRESS':                     label = "En cours";                         break;
      case 'USER_HAS_PARTICIPATED':           label = "A participé";                      break;
      case 'CANCELLED':                       label = "Annulé / refusé / non finalisé";   break;
      default:                                label = "";                                 break;
    }
    return label;
  }

  public getColor(status:string): string{
    let color:string;
    switch (status) {
      case 'APPLICATION':                     color = "#FF000080";    break;
      case 'DISCUSSION':                      color = "#FF0000";      break;
      case 'WAITING_ASSOCIATION_VALIDATION':  color = "#FFC000";      break;
      case 'WAITING_MANAGER_VALIDATION':      color = "#FFC00080";    break;
      case 'IN_PROGRESS':                     color = "#92D050";      break;
      case 'USER_HAS_PARTICIPATED':           color = "#D8D8D8";      break;
      case 'CANCELLED':                       color = "#D8D8D8";      break;
      default:                                color = "#D8D8D8";      break;
    }
    return color;
  }

  private resfreshState(): void {
    this.state$.next({
      stats: this.stats,
      currentStatus: this.currentStatus,
      totalCount:this.totalCount,
      loading: this.loading,
    });
  }
}
