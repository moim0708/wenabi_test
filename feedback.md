# Feedback

Thank you for taking the test, we are very happy that you dedicate some time for Wenabi 💛.

**_How many hours did you work on this mini project ?_**
Jour 1 : 1h30
Jour 2 : 5h30
Jour 3 : 7h00
Jour 4 : 3h00
Jour 1 : 2h30

Total : 19h30
---

## You have something else to say ?
- Le Background jaune bug sur Safari 

Before submitting your final work, tell us what you think about these different topics:

**Note**: It is totally **OK** for you to not have any feedback. We simply wanted to give a space for anyone who feels that this process can be improved and we are happy to hear it. Also, there are no right/wrong answers !

**_1 - How do you feel about the workload that was necessary to complete the mini project ? Would you say it was too much or too little ?_**

**_2 - How would you change this exam and/or examination process?_**
